'use strict';

var moment = require('moment-timezone');

var PunchModel = require('../../models/punch');


module.exports = function (router) {

    var model = new PunchModel();


    router.get('/', function (req, res) {
        
    	/*var punchInstance = new PunchModel({ employeeName: 'Raymond', workDate: new Date(), commission:1.00, salary:2.00 });
		punchInstance.save(function(err) {
		  // we've saved the dog into the db here
		  if (err) throw err;
		  console.log('saved punchInstance');
		});	*/
		var searchCriteria = {};
    	if (req.cookies.employeeName){
			searchCriteria = {employeeName:req.cookies.employeeName};
    	}else{
    		searchCriteria = {employeeName:''};
    	}

		var punchs = PunchModel.find(searchCriteria).sort({'workDate':-1}).exec(function(err,data){
			res.render('punch/index',{records:data});
		});
        
        
    });


    router.post('/delete', function (req, res) {
        var id = req.body.id;        

        if (id){        	
   			PunchModel.remove({_id:id},function(err) {
			  	// we've saved the dog into the db here
			 	if (err) { throw err ; }			  

			  	res.redirect('/punch');
			});
		}else{			
			res.redirect('/punch');
		}	
    });

    router.post('/', function (req, res) {
        var data = req.body;        

        if (data.employeeName){
        	//
	    	var punchInstance = new PunchModel({ employeeName: data.employeeName.trim() , workDate: moment.tz(data.workDate, 'YYYY-MM-DD').toDate() , commission:data.commission, salary:data.salary });

			punchInstance.save(function(err) {
			  	// we've saved the dog into the db here
			 	if (err) { throw err ; }
			  	res.cookie('employeeName', data.employeeName);

			  	res.redirect('/punch');
			});        
		}else{
			res.redirect('/punch');
		}	
    });

    router.get('/create', function (req, res) {
        console.log('Cookies: ', req.cookies);

        var now = moment();
        var today = now.format('YYYY-MM-DD');


    	res.render('punch/create',{employeeName: req.cookies.employeeName, today:today });
                
    });

};
