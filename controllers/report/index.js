'use strict';

var ReportModel = require('../../models/report');
var PunchModel = require('../../models/punch');

var moment = require('moment-timezone');

module.exports = function (router) {

    var model = new ReportModel();

    router.get('/', function (req, res) {

        res.send('<code><pre>' + JSON.stringify(model, null, 2) + '</pre></code>');
        
    });

    router.get('/summary', function (req, res) {

		var records=[];
		var searchCriteria = {};

		if (req.cookies.employeeName){
			searchCriteria = {employeeName:req.cookies.employeeName};
    	}else{
    		searchCriteria = {employeeName: ''};
    	}

		searchCriteria.workDate = {$gte: moment('2014-11-21', "YYYYMMDD").toDate(), $lt: moment('2014-12-21', "YYYYMMDD").toDate()};
    	generateReport( searchCriteria ,function(err, data){

			if (err) { return console.log(err);}			

			if (data.length>0){
				populateRecords('Nov 2014', data[0],records);
		    }

			searchCriteria.workDate = {$gte: moment('2014-12-21', "YYYYMMDD").toDate(), $lt: moment('2015-1-21', "YYYYMMDD").toDate()};
    		generateReport( searchCriteria ,function(err, data){

    			if (err) { return console.log(err);}

				if (data.length>0){
				populateRecords('Dec 2014', data[0],records);
		        }

    			searchCriteria.workDate = {$gte: moment('2014-12-21', "YYYYMMDD").toDate(), $lt: moment('2015-1-21', "YYYYMMDD").toDate()};
	    		generateReport( searchCriteria ,function(err, data){

	    			if (err) { return console.log(err);}

					if (data.length>0){
					populateRecords('Jan 2015', data[0],records);
			        }

	    			searchCriteria.workDate = {$gte: moment('2015-1-21', "YYYYMMDD").toDate(), $lt: moment('2015-2-21', "YYYYMMDD").toDate()};
		    		generateReport( searchCriteria ,function(err, data){

		    			if (err) { return console.log(err);}

						if (data.length>0){
						populateRecords('Feb 2015', data[0],records);
				        }

		    			searchCriteria.workDate = {$gte: moment('2015-2-21', "YYYYMMDD").toDate(), $lt: moment('2015-3-21', "YYYYMMDD").toDate()};
			    		generateReport( searchCriteria ,function(err, data){

			    			if (err) { return console.log(err);}

							if (data.length>0){
							populateRecords('Mar 2015', data[0],records);
					        }

			    			res.render('report/summary',{records:records});
			    		});
		    		});
	    		});
    		});
    	});
	    
    });

    function generateReport(searchCriteria, callback){
    	    	
		PunchModel.aggregate([
			{ $match: searchCriteria },
			{ $group : { _id : {employeeName:'$employeeName' } ,salary : { $sum : '$salary' } ,commission : { $sum : '$commission' }} },
    		{ $sort : { '_id.employeeName' : 1} },
    		{ $limit : 5 }
	    ],callback);
    }

    function populateRecords(reportName, record, records){
	var result = {};
		result.reportName=reportName;
	result.salary = record.salary;
	result.commission = record.commission;
	result.employeeName = record._id.employeeName
	records.push(result);
    }

};
