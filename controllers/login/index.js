'use strict';


var LoginModel = require('../../models/login');


module.exports = function (router) {

    var model = new LoginModel();


    router.get('/', function (req, res) {
            
		res.render('login/index',{});
        
    });

    router.post('/', function (req, res) {

        res.cookie('employeeName', req.body.employeeName, { maxAge:  365 * 24 * 60 * 60 * 1000 , httpOnly: true, expires:  false});

		res.redirect('punch');
        
    });

};
