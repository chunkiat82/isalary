'use strict';

/*requirejs.config({
  paths: {    
  }
});

require(['../components/jquery/dist/jquery'
    ,'../components/foundation/js/foundation.min'
    ,'../components/foundation/js/foundation/foundation.slider'
    ,'../components/moment/min/moment.min'
    ,'../components/modernizr/modernizr'], function () {

    var app = {
        initialize: function () {
            $(function() {
                $(document).foundation();
            });
        }
    };

    app.initialize();

});*/

require.config({    
    paths: {
    },
    shim: {       
        'underscore': {
                exports: '_'
        }
    },
});


require(['../components/jquery/dist/jquery'], function () {
    require(['../components/foundation/js/foundation.min'], function () {
        require(['../components/foundation/js/foundation/foundation.slider'], function () {
            //jq.noConflict(true);
            $(function() {
                $(document).foundation();
            });
            return true;
        });
    });
});