'use strict';

var mongoose = require('mongoose');

var punchModel = function PunchModel() {

	var punchSchema = mongoose.Schema({
        workDate: Date,
        employeeName: String,        
        salary: Number,
        commission: Number
    });

    return mongoose.model('punch', punchSchema);
};

module.exports =  new punchModel();