'use strict';

var mongoose = require('mongoose');

var reportModel = function ReportModel() {

	var reportSchema = mongoose.Schema({
        employeeName: String,
        reportName: String,    
        startDate: Date,        
        endDate: Date
    });

    return mongoose.model('report', reportSchema);
};

module.exports =  new reportModel();